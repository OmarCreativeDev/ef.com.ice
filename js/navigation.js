$(document).ready(function() {

	var nav = $('nav');
	var navButtons = $('nav button');
	var firstButtonRightPosition = $('nav li:nth-child(2)').position().left;

	nav.prepend('<div class="selectedNav" style="width:'+firstButtonRightPosition+'px"></div>');
	var selectedNav = $('nav .selectedNav');

	navButtons.click(function() {

		var buttonWidth = $(this).outerWidth();
		var buttonLeftPosition = $(this).position().left;
		var buttonRightPosition = buttonLeftPosition + buttonWidth;
		
		selectedNav.animate({'width': buttonRightPosition}, 1000);

		if ( !$(this).hasClass('show') ) {

			var getClass = $(this).attr('class');
			var contentSections = $('#Info section');

			$(navButtons.selector).removeClass('show');
			$(this).addClass('show');

			$(contentSections.selector).removeClass('show');
			$(contentSections.selector + '.' + getClass).addClass('show');

		}	

	});

	$(window).resize(function() {
		
		var currentButton = $('nav button.show');
		var lastButtonWidth = $('nav li:last-child button').outerWidth();

		var getNextButton = $(currentButton.selector).parent();
		var currentButtonRightPositon = getNextButton.next().length > 0 ? getNextButton.next().position().left : getNextButton.position().left + lastButtonWidth;

		selectedNav.css({'width': currentButtonRightPositon});

	});	

});