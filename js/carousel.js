$(document).ready(function() {

	var carouselWrapper = $('#Carousel');
	var carousel = $('.canvas ul'); // Set canvas element as variable
	var slides = $(carousel.selector + ' li'); // Set canvas slides as variable
	var slideDuration = 5000; // Set slide duration
	var secondImageTimeout = 2000; // Slides second image timeout duration
	var slideWidth = 1360; // Set slide width
	var getSlideCount = carousel.children().length // Calculate number of slides
	var setCarouselWidth = getSlideCount * slideWidth; // Calculate carousel width
	
	slides.each(function(i){
		$(this).attr('data-index',i); // Add data index attribute to each slide
		$(this).find('img:nth-child(2)').css({'left':slideWidth}); // Move second image of slide out of carousel viewpoint
	});

	carousel.css('width', setCarouselWidth); // Set carousel width css
	$(slides.selector+(":first")).addClass('current'); // Add current class to first slide
	carouselWrapper.addClass('slide0'); // Add class to carousel wrapper for background image swap
	carousel.animate({'left':'200px'}); // Move first slide in to viewpoint

	setTimeout(function(){
		$(slides.selector+(':first img:nth-child(2)')).animate({'left':'-='+slideWidth});
	}, secondImageTimeout);

	function slideCarousel() {
		
		// Find and remove current class from slide/s
		var removeCurrentClass = carousel.find('.current').removeAttr('class');
		var newCurrentClass = removeCurrentClass.next().length > 0 ?  removeCurrentClass.next().addClass('current') : $(slides.selector+(":first")).addClass('current');
		
		var thisIndex = newCurrentClass.attr('data-index');
		var removeCarouselClass = carouselWrapper.removeAttr('class');
		var newCarouselClass = carouselWrapper.addClass('slide'+thisIndex);

		var nextSlide = removeCurrentClass.next().length > 0 ? carousel.animate({'left':'-='+slideWidth}) : carousel.animate({'left':'200px'});

		setTimeout(function(){
			$(slides.selector+'[data-index="'+thisIndex+'"] img:nth-child(2)').animate({'left':'-='+slideWidth});
		}, secondImageTimeout);

		$(slides.selector+'[data-index="'+thisIndex+'"] img:nth-child(2)').css({'left':slideWidth});

	}

	setInterval(slideCarousel, slideDuration);
				
});