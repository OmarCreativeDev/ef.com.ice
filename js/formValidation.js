$(document).ready(function() {

	entryForm = $('#EntryForm form');

	entryForm.submit(function() {

		var isPortuguese = location.pathname.split("/")[1] == "br";
		var isChinese = location.pathname.split("/")[1] == "cn";
		var isGerman = location.pathname.split("/")[1] == "de";
		var isEnglish = location.pathname.split("/")[1] == "en";
		var isSpanish = location.pathname.split("/")[1] == "es";
		var isFrench = location.pathname.split("/")[1] == "fr";
		var isItalian = location.pathname.split("/")[1] == "it";
		var isRussian = location.pathname.split("/")[1] == "ru";

		// get form element values + trim white space and store as variables
		var firstName = $.trim( $('#FirstName').val() );
		var lastName = $.trim( $('#LastName').val() );
		var emailAddress = $.trim( $('#EmailAddress').val() );		
		var country = $('#Country option:selected').val();
		var companyName = $.trim( $('#CompanyName').val() );
		var jobTitle = $('#JobTitle option:selected').val();
		var phoneNumber = $.trim( $('#PhoneNumber').val() );
		var filesUpload = $('.filesUpload').val();

		// Validate!
		if ( (firstName.length) && (lastName.length) && (emailAddress.length) && (country != 'Select country') && (companyName.length) && (jobTitle != 'Select job title') && (phoneNumber.length) && (filesUpload != '') ) {
			return true;
		} else {

			if (isPortuguese) {
				alert('Por favor, preencha todas as partes do formulário.');
				return false;
			}
			else if (isChinese) {
				alert('请完成所有部件的形式。');
				return false;
			}
			else if (isGerman) {
				alert('Bitte füllen Sie alle Teile des Formulars.');
				return false;
			}			
			else if (isEnglish) {
				alert('Please complete all parts of the form.');
				return false;
			}
			else if (isSpanish) {
				alert('Por favor complete todo el formulario.');
				return false;
			}
			else if (isFrench) {
				alert("S'il vous plaît remplir toutes les parties du formulaire.");
				return false;
			}
			else if (isItalian) {
				alert('Si prega di compilare tutte le parti del modulo.');
				return false;
			}									
			else if (isRussian) {
				alert('Пожалуйста, заполните все части формы.');
				return false;
			}
			else {
				alert('Please complete all parts of the form.');
				return false;
			}

		}

	});

});