$(document).ready(function() {

	var form = $('.updatePage form');
	var formButtons = $('.updatePage ul button');

	formButtons.click(function() {

		if ( !$(this).hasClass('show') ) {

			var getClass = $(this).attr('class');
			var formContentSections = $('.updatePage form section');

			$(formButtons.selector).removeClass('show');
			$(this).addClass('show');

			$(formContentSections.selector).removeClass('show');
			$(formContentSections.selector + '.' + getClass).addClass('show');

		}

	});

});