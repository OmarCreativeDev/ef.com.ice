<?php

// head variables
$title="EF International Collaboration Awards";
$metaKeywords="collaboration, collaboration awards, international collaboration awards, corporate award, International business award, international business success";
$metaDescription="The EF International Collaboration Awards recognize the achievements of companies with regard to international collaboration and cross-border communication projects. Apply here.";
$socialMediaURL="http://www.ef.com/ice";
$socialMediaDescription="As leaders in the provision of strategic English language training, and a truly international company itself, EF has a keen interest in promoting such cross-border business collaboration. For this reason, we have founded an award to recognise the achievements of companies with regard to international collaboration and cross-border communication projects.";

// header variables
$logoHeader="ICE International Collaboration Excellence";
$logoAltTag="EF Logo";

// carousel variable
$carouselHeader="The EF International Collaboration Awards 2013";

// navigation variables
$navButton1="About the awards";
$navButton2="Who should enter?";
$navButton3="Why enter?";
$navButton4="How to enter";

// content section variables
$contentSection1="<p>Globalization is increasingly shaping the business agenda, such that it is becoming more important than ever to foster a truly internationalized workforce; one that can operate seamlessly across geographical boundaries to capitalize on all business opportunities.</p>
<p>As leaders in the provision of strategic English language training, and a truly international company itself, EF has a keen interest in promoting such cross-border business collaboration. For this reason, we have founded an award to recognise the achievements of companies with regard to international collaboration and cross-border communication projects.</p>
<p>We believe such projects represent the future norm for successful businesses, which is why we think it is important to highlight best practice in this area. We hope it will stimulate further successful international projects and help to foster a truly global business culture.</p>";
$contentSection2="<p>The winning organisation will receive a residential stay for 2 Executives at one of EF’s Executive Language institute in either Boston (US) or Cambridge (UK), worth $6,500.</p>
<p>Two executives of your choice will have the opportunity to perfect their business English by attending our most popular week-long residential English course – the Combination Course. Through a mixture of private, semi-private and group classes, the Executives will strengthen their conversational and written English, while at the same time, honing their pronunciation and expanding their vocabulary.</p>  
<p>Your company will also receive global recognition as a successful international organization through the EF Education First network and publicity channels, giving you exposure to thousands of companies worldwide.</p>";
$contentSection3="<p>This award is open to any size or type of company, the only criterion is that you have successfully implemented a project that involved the engagement of employees in multiple countries to achieve outstanding results.</p>
<p>Whether it was a product development project that was conceived in the boardroom in the US, brought to life by a team of designers in Brazil and China and then brought to market by teams in Germany, Italy and France; or a consultancy project involving the collaboration and collective knowledge of local teams across Europe, we would love to hear about it.</p> 
<p>Each entry will be carefully judged on its own merits by our expert panel of judges who come from a diverse range of backgrounds, from international business experts to academic professors.</p>";
$contentSection4="<p>To enter, you need to submit a word document (maximum 1000 words) that details your international collaboration project. This should be split into 
the sections below:</p>
<ol>
	<li>Project background, scope and scale - how did it arise, what were the aims? How many countries &amp; individuals were involved?</li>
	<li>How was international collaboration central to this project?</li>
	<li>Processes and procedures – What factors were key to the project&rsquo;s success? What lessons were learned? What would you do differently next time?</li>
	<li>Measures of success – What were these? (Quantitative where possible). Include comments from those involved in the project where possible.</li>
	<li>Lasting impact – what were the wider implications of the project if any?</li>
</ol>
<p>All entries must be received by April 1st 2014. The shortlist of companies will be announced on May 1st 2014, and the winner notified in June 2014.</p>";

// form variables
$formHeader="Entry form";
$formFirstName="First name";
$formLastName="Last name";
$formEmailAddress="Email address";
$formCompanyName="Company name";
$formPhoneNumber="Phone number";
$formInstructions="Please attach a Word or text document that outlines your project. You may also attach images that support your application. 1 MB file limit per file.";
$formSubmitButton="Submit";

// entry terms
$entryTermsHeader="Terms and conditions for entry:";
$entryTermsContent="<ol>
<li>All entries must be received by 5pm GMT on 1st April 2014.</li>
<li>All entries must include explanatory text that supports the placement, otherwise the nomination will be considered void.</li>
<li>Submission to the awards is free of charge.</li>
<li>Companies can submit more than one project if desired, up to a maximum of three per company.</li> 
<li>Nominating companies are expected to provide accurate information about their company’s ventures. If, during the judging process, it is discovered that inappropriate claims have been made about the ventures, said information will be removed from the nomination and a warning will be given.</li>
<li>The judges’ decision is final.</li>
</ol>";

// facebook variables
$facebookURL="https://www.facebook.com/sharer/sharer.php?u=http://www.ef.com/ice";
$facebookTitle="EF | ICE Facebook";
$facebookAltTag="Facebook Icon";

// email variables
$emailURL="mailto:?subject=International%20Collaboration%20Awards&amp;body=Hi%0A%0A%20%0AI%20came%20across%20an%20award%20that%20your%20business%20may%20be%20interested%20in%20entering.%20The%20EF%20International%20Collaboration%20Excellence%20(ICE)%20awards%20recognize%20the%20achievements%20of%20companies%20with%20regard%20to%20international%20collaboration%20and%20cross-border%20communication%20projects.%0A%20%0A%0ATake%20a%20look%2C%20you%20can%20enter%20the%20awards%20here%20-%20http%2F%2Fwww.ef.com%2Fice.";
$emailTitle="EF | ICE Email";
$emailAltTag="Email Icon";

// linkedin variables
$linkedinURL="http://www.linkedin.com/shareArticle?mini=true&title=EF%20International%20Collaboration%20Awards&url=http://www.ef.com/ice&summary=As%20leaders%20in%20the%20provision%20of%20strategic%20English%20language%20training%2C%20and%20a%20truly%20international%20company%20itself%2C%20EF%20has%20a%20keen%20interest%20in%20promoting%20such%20cross-border%20business%20collaboration.%20For%20this%20reason%2C%20we%20have%20founded%20an%20award%20to%20recognise%20the%20achievements%20of%20companies%20with%20regard%20to%20international%20collaboration%20and%20cross-border%20communication%20projects.";
$linkedinTitle="EF | ICE LinkinIn";
$linkedinAltTag="LinkinIn Icon";

// twitter variables
$twitterURL="http://twitter.com/share?text=EF%20International%20Collaboration%20Awards&url=http://www.ef.com/ice";
$twitterTitle="EF | ICE Twitter";
$twitterAltTag="Twitter Icon";

?>