<?php

	require 'PHPMailerAutoload.php';

	$mail = new PHPMailer;

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.gmail.com';  					  // Specify main and backup server
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'ice.awards.ef@gmail.com';          // SMTP username
	$mail->Password = 'iceawardsef';                      // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
	$mail->Port       = 587; 

	$mail->From = $_POST['EmailAddress'];
	$mail->FromName = $_POST['FirstName'] . ' ' . $_POST['LastName'];
	$mail->addAddress('Sarah.Newcombe@EF.com');  		  // Add a recipient
	$mail->addReplyTo($_POST['EmailAddress'], $_POST['FirstName'] . ' ' . $_POST['LastName']);

	$mail->WordWrap = 200;                                // Set word wrap via character count
	$mail->addAttachment($file1TempName, $file1Name);     // Add attachments
	$mail->addAttachment($file2TempName, $file2Name);     // Add attachments
	$mail->addAttachment($file3TempName, $file3Name);     // Add attachments
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = 'ICE Awards Entry Form : ' . $_POST['FirstName'] . ' ' . $_POST['LastName'] . ' [Company: ' . $_POST['CompanyName'] . ']';

	$mail->Body = '<p><b>Name</b></br>' . $_POST['FirstName'] . ' ' . $_POST['LastName'] . '</p>' .
			      '<p><b>Email Address</b></br>' . $_POST['EmailAddress'] . '</p>' .
			      '<p><b>Country</b></br>' . $_POST['Country'] . '</p>' .
			      '<p><b>Company Name</b></br>' . $_POST['CompanyName'] . '</p>' .
			      '<p><b>Job Title</b></br>' . $_POST['JobTitle'] . '</p>' .
			      '<p><b>Phone Number</b></br>' . $_POST['PhoneNumber'] . '</p>' .
			      '<p><b>Filenames</b></br>' . $file1Name . '</br>' . $file2Name . '</br>' . $file3Name . '</p>';

	$mail->AltBody = 'Name : ' . $_POST['FirstName'] . ' ' . $_POST['LastName'] . "\r\n" .
				     'Email Address : ' . $_POST['EmailAddress'] . "\r\n" .
				     'Country : ' . $_POST['Country'] . "\r\n" .
				     'Company Name : ' . $_POST['CompanyName'] . "\r\n" .
				     'Job Title : ' . $_POST['JobTitle'] . "\r\n" .
				     'Phone Number : ' . $_POST['PhoneNumber'] . "\r\n" .
				     'Filenames : ' . $file1Name . '   ' . $file2Name . '   ' . $file3Name;

	if(!$mail->send()) {
	   echo 'Message could not be sent.';
	   echo 'Mailer Error: ' . $mail->ErrorInfo;
	   exit;
	}

	echo 'Message has been sent';

?>