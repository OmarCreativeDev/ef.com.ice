<?php

	// get temp file paths
	$file1TempName=$_FILES['FilesUpload1']['tmp_name'];
	$file2TempName=$_FILES['FilesUpload2']['tmp_name'];
	$file3TempName=$_FILES['FilesUpload3']['tmp_name'];

	// get uploaded file names
	$file1Name=$_FILES['FilesUpload1']['name'];
	$file2Name=$_FILES['FilesUpload2']['name'];
	$file3Name=$_FILES['FilesUpload3']['name'];

	// get uploaded file extension
	$file1Ext = substr($file1Name, strrpos($file1Name, '.') + 1);
	$file2Ext = substr($file2Name, strrpos($file2Name, '.') + 1);
	$file3Ext = substr($file3Name, strrpos($file3Name, '.') + 1);
	$fileExtensionsArray = array_unique(array_filter(array( $file1Ext, $file2Ext, $file3Ext )));

	// get uploaded file mimes
	$file1Mime=$_FILES['FilesUpload1']['type'];
	$file2Mime=$_FILES['FilesUpload2']['type'];
	$file3Mime=$_FILES['FilesUpload3']['type'];
	$fileMimesArray = array_unique(array_filter(array( $file1Mime, $file2Mime, $file3Mime )));

	// get uploaded file sizes
	$file1Size=$_FILES['FilesUpload1']['size'];
	$file2Size=$_FILES['FilesUpload2']['size'];
	$file3Size=$_FILES['FilesUpload3']['size'];
	$fileSizesArray = array_unique(array_filter(array( $file1Size, $file2Size, $file3Size )));	

	// allowed file extensions	
	$allowedExtensions = array('gif', 'jpeg', 'jpg', 'png',	'doc', 'docx', 'rtf', 'txt', 'pdf');

	// allowed file mimes
	$allowedMimes = array('image/gif', 'image/jpeg', 'image/jpg', 'image/pjpeg', 'image/x-png', 'image/png', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/rtf', 'text/plain', 'application/pdf');
	
	// allowed file size
	$allowedFileSize = 1048576;

	// check all 3 file uploads are of the right file extension, mime type, if file fileSizesArray == Array & check file sizes
	if ( (count(array_intersect($allowedExtensions, $fileExtensionsArray)) == count($fileExtensionsArray)) 
		&& (count(array_intersect($allowedMimes, $fileMimesArray)) == count($fileMimesArray))
		&& ( !empty($fileSizesArray)) && ( max($fileSizesArray) < $allowedFileSize) ) {

		// Determine the path to which we want to save this file
		$newFilePath = dirname(__FILE__).'/../entries/'.$_POST["CompanyName"].'-';	
		$newFile1Location = $newFilePath.$file1Name;
		$newFile2Location = $newFilePath.$file2Name;
		$newFile3Location = $newFilePath.$file3Name;

		// Check if the file with the same name is already exists on the server
		if ( (!file_exists($newFile1Location)) && (!file_exists($newFile2Location)) && (!file_exists($newFile3Location)) ) {

			// send email
			include ("entryFormEmailer.php");

			move_uploaded_file($file1TempName, $newFile1Location);
			move_uploaded_file($file2TempName, $newFile2Location);
			move_uploaded_file($file3TempName, $newFile3Location);
			
			echo "<h2>It's done! Your file/s has been saved.</h2>";

			?> <script> setTimeout("location.href = 'thankyou.html';",4000); </script> <?php

		} else {
			echo "<h2>Error: File/s already exists</h2>";
			?> <script> setTimeout("location.href = 'index.html';",4000); </script> <?php
		}

	}

	else {
		echo "<h2>Error: Only .gif, .jpeg, .jpg, .png, .doc, .docx, .rtf, .txt, .pdf files under 1 MB are accepted for upload.</h2>";
		?> <script> setTimeout("location.href = 'index.html';",4000); </script> <?php
	}

?>